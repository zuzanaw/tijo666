package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CounterDto;

@Component
public class CounterMapper {

    public CounterDto mapToCounter(Long counter) {
        CounterDto counterDto = new CounterDto();
        counterDto.setCounter(counter);

        return counterDto;
    }

}
