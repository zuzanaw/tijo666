package pl.edu.pwsztar.domain.dto;

public class CounterDto {
    private long counter;

    public CounterDto() {}

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }
}
